let fs = require("fs")
let path = require("path")

function callback2(boardId, callback) {
    setTimeout(() => {
        fs.readFile(path.join(__dirname, `lists.json`), { encoding: 'utf-8' }, (err, data) => {
            if (err) {
                callback(err)
            } else {
                let boardlist
                data = JSON.parse(data)
                boardlist = data[boardId]
                if (typeof (boardlist) === 'undefined') {
                    cards = `No board found for ${boardId}`
                }

                callback(null, boardlist)
            }
        })
    }, 2 * 1000)

}

module.exports = callback2