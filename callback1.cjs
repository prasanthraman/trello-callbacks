let fs = require("fs")
let path = require("path")

function callback1(boardId, callback) {
    setTimeout(() => {
        fs.readFile(path.join(__dirname, `boards.json`), { encoding: 'utf-8' }, (err, data) => {
            if (err) {
                callback(err)
            } else {
                data = JSON.parse(data)
                    .find((record) => {

                        return (record.id == boardId)
                    })
                if (typeof (data) === 'undefined') {
                    data = `No data found for ${boardId}`
                }
                callback(null, data)
            }
        })

    }, 2 * 1000)

}

module.exports = callback1
