let fs = require("fs")
let path = require("path")

let callback1 = require("./callback1.cjs")
let callback2 = require("./callback2.cjs")
let callback3 = require("./callback3.cjs")


function callback6() {
    setTimeout(() => {
        
        fs.readFile(path.join(__dirname, 'boards.json'), { encoding: 'utf-8' }, (err, data) => {
            if (err) {
                console.log(err)
            } else {
                data = JSON.parse(data)
                    .find((record) => {
                        return record.name == 'Thanos'
                    })
                callback1(data.id, (err, thanosBoard) => {
                    if (err) {
                        console.log(err)
                    } else {
                        console.log(thanosBoard)
                        callback2(thanosBoard.id, (err, listsOfThanos) => {
                            if (err) {
                                console.log(err)
                            } else {
                                console.log(listsOfThanos)

                            }

                        })
                    }
                })
            }
        })


        fs.readFile(path.join(__dirname, 'lists.json'), { encoding: 'utf-8' }, (err, data) => {
            if (err) {
                console.log(err)
            } else {
                data = JSON.parse(data)
                data = Object.entries(data)
                    .reduce((listIds, [, value]) => {
                        value = value.reduce((acc, record) => {
                            acc.push(record.id)

                            return acc
                        }, [])

                        listIds = listIds.concat(value)

                        return listIds
                    }, [])

                data.forEach((listId) => {
                    callback3(listId, (err, cards) => {
                        if (err) {
                            console.log(err)
                        } else {
                            console.log(cards)
                        }
                    })
                })
            }
        })

    }, 2 * 1000)
}
module.exports = callback6