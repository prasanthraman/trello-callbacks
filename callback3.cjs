let fs = require("fs")
let path = require("path")

function callback3(listId, callback) {
    setTimeout(() => {
        fs.readFile(path.join(__dirname, `cards.json`), { encoding: 'utf-8' }, (err, data) => {
            if (err) {
                callback(err)
            } else {
                let cards
                data = JSON.parse(data)
                cards = data[listId]
                if (typeof (cards) === 'undefined') {
                    cards = `No cards found for ${listId}`
                }
                callback(null, cards)
            }
        })
    }, 2 * 1000)

}

module.exports = callback3